import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Person person1 = new Person();
        person1.setFirstName("hung");
        System.out.println(person1.toString());
        Person person2 = new Person("Viet","Hung","Male","0978894158","hungnv@gmail.com");

        System.out.println(person2.toString());
    }
}
