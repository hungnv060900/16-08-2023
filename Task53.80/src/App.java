import java.text.DecimalFormat;
import java.util.Random;

public class App {

    // Task 1: Làm tròn n số sau dấu .
    public static double roundNumber(double num, int n) {
        return Math.round(num * Math.pow(10, n)) / Math.pow(10, n);
    }

    // Task 2: Lấy 1 số random bất kỳ trong khoảng 2 số cho trước
    public static double randomNumberInRange(double start, double end) {
        Random random = new Random();
        return start + (end - start) * random.nextDouble();
    }

    // Task 3: Tính số Pythagoras từ 2 số đã cho
    public static double pythagoras(double a, double b) {
        return Math.sqrt(a * a + b * b);
    }

    // Task 4: Kiểm tra số đã cho có phải số chính phương hay không
    public static boolean isPerfectSquare(double num) {
        double sqrt = Math.sqrt(num);
        return sqrt == Math.floor(sqrt);
    }

    // Task 5: Lấy số lớn hơn gần nhất của số đã cho chia hết cho 5
    public static int nextMultipleOf5(int num) {
        return ((num / 5) + 1) * 5;
    }

    // Task 6: Kiểm tra đầu vào có phải số hay không
    public static boolean isNumber(String inputStr) {
        try {
            Double.parseDouble(inputStr);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    // Task 7: Kiểm tra số đã cho có phải lũy thừa của 2 hay không
    public static boolean isPowerOfTwo(int num) {
        return (num > 0) && ((num & (num - 1)) == 0);
    }

    // Task 8: Kiểm tra số đã cho có phải số tự nhiên hay không
    public static boolean isNaturalNumber(double num) {
        return num >= 0 && num == (int) num;
    }

    // Task 9: Thêm dấu , vào phần nghìn của mỗi số
    public static String addCommasToNumber(double num) {
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        return formatter.format(num);
    }

    // Task 10: Chuyển số từ hệ thập phân về hệ nhị phân
    public static String decimalToBinary(int num) {
        return Integer.toBinaryString(num);
    }

    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        // Task 1: Làm tròn n số sau dấu .
        System.out.println(roundNumber(2.100212, 2));
        System.out.println(roundNumber(2.100212, 3));

        // Task 2: Lấy 1 số random bất kỳ trong khoảng 2 số cho trước
        System.out.println(randomNumberInRange(1, 10));

        // Task 3: Tính số Pythagoras từ 2 số đã cho
        System.out.println(pythagoras(2, 4));

        // Task 4: Kiểm tra số đã cho có phải số chính phương hay không
        System.out.println(isPerfectSquare(64));

        // Task 5: Lấy số lớn hơn gần nhất của số đã cho chia hết cho 5
        System.out.println(nextMultipleOf5(32));

        // Task 6: Kiểm tra đầu vào có phải số hay không
        System.out.println(isNumber("12"));

        // Task 7: Kiểm tra số đã cho có phải lũy thừa của 2 hay không
        System.out.println(isPowerOfTwo(16));

        // Task 8: Kiểm tra số đã cho có phải số tự nhiên hay không
        System.out.println(isNaturalNumber(1000));

        // Task 9: Thêm dấu , vào phần nghìn của mỗi số
        System.out.println(addCommasToNumber(10000.23));

        // Task 10: Chuyển số từ hệ thập phân về hệ nhị phân
        System.out.println(decimalToBinary(51));
    }
}
