package model;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class Order2 {
    int id;
    String customerName;
    long price;
    Date orderDate;
    boolean confirm;
    String[] items;
    Person buyer;

    public Order2(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 1200;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "Book", "pen", "ruler" };
        this.buyer = new Person("HungNV", 23, 55);
    }

    public Order2(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items,
            Person buyer) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = new String[] { "Book", "pen", "ruler" };
        this.buyer = new Person("HungNV");
    }

    public Order2() {
        this("2");
    }

    public Order2(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[] { "Book", "pen", "ruler" },
                new Person("customerName"));
    }

    @Override
    public String toString() {
        Locale.setDefault(new Locale("vi", "VN"));
        // Định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defautTimeFormatter = DateTimeFormatter.ofPattern(pattern); // Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        // return (trả ra) chuỗi (string)
        return "Order [id=" + id
                + ", customerName=" + customerName
                + ", price=" + usNumberFormat.format(price)
                + ", orderDate="
                + defautTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                + ", confirm: " + confirm
                + ", items=" + Arrays.toString(items)
                + ", byper=" + buyer;
    }
}
