import java.util.ArrayList;
import java.util.Date;

import model.Order2;
import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");

         ArrayList<Order2> arrayList = new ArrayList<>();
        Order2 order1 =new Order2();
        Order2 order2 = new Order2("Hung");
        Order2 order3 = new Order2(3, "Hung1", 600000);
        Order2 order4= new Order2(4, "Hung2", 800000, new Date(), false,new String[]{"hop mau","tay","giay"},new Person("null", 0, 0) );
        arrayList.add( order1);
        arrayList.add( order2);
        arrayList.add( order3);
        arrayList.add( order4);
        for (Order2 order : arrayList) {
            System.out.println(order.toString());
        }
    }
}
