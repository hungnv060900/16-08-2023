package model;

public class User {
    String password;
    String username;
    String enabled;

    
    public User() {
    }
    
    public User(String password, String username, String enabled) {
        this.password = password;
        this.username = username;
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getEnabled() {
        return enabled;
    }
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "User [password=" + password + ", username=" + username + ", enabled=" + enabled + "]";
    }
    
}
