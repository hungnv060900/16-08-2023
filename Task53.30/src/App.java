import java.util.ArrayList;

import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
       // System.out.println("Hello, World!");
       ArrayList<Person> arrayList = new ArrayList<>();
       //Khoi tao person voi cac tham so
       Person person0 = new Person();
       Person person1 = new Person("Devcamp");
       Person person2 = new Person("Hung", 23, 55);
       Person person3= new Person("Toan", 20, 60, 50000000, new String[]{"Little game"});
       //add vo list
       arrayList.add( person0);
       arrayList.add( person1);
       arrayList.add( person2);
       arrayList.add( person3);
       for (Person person : arrayList) {
        System.out.println(person.toString());
       }
       
    }
}
