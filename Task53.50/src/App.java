import java.util.Date;
import java.util.ArrayList;

import model.Order;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        ArrayList<Order> arrayList = new ArrayList<>();
        Order order1 =new Order();
        Order order2 = new Order("Hung");
        Order order3 = new Order(3, "Hung1", 600000);
        Order order4= new Order(4, "Hung2", 800000, new Date(), false,new String[]{"hop mau","tay","giay"} );
        arrayList.add( order1);
        arrayList.add( order2);
        arrayList.add( order3);
        arrayList.add( order4);
        for (Order order : arrayList) {
            System.out.println(order.toString());
        }
    }
}
